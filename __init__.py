# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import analysis_package
from . import configuration
from . import category
from . import service_order
from . import party
from . import invoice
# from . import api_log
from . import product
from . import voucher
from . import purchase
from . import ir
from . import move


def register():
    Pool.register(
        analysis_package.AnalysisPackage,
        analysis_package.AnalysisPackageLine,
        configuration.Configuration,
        category.Category,
        party.Party,
        service_order.Order,
        service_order.OrderLine,
        service_order.OrderPayment,
        service_order.Move,
        service_order.DetailedBillingAnalysisStart,
        service_order.AddPackageStart,
        invoice.InvoiceLaboratoryOrder,
        invoice.Invoice,
        invoice.InvoiceLine,
        # api_log.ApiLog,
        service_order.CreateInvoiceStart,
        product.Section,
        product.Conservation,
        product.Template,
        product.ProductSupplier,
        voucher.Voucher,
        purchase.Purchase,
        purchase.Line,
        ir.Cron,
        move.Move,
        module='laboratory', type_='model')
    Pool.register(
        service_order.CreateInvoice,
        service_order.PrintServiceOrder,
        service_order.DetailedBillingAnalysisWizard,
        service_order.AddPackage,
        module='laboratory', type_='wizard')
    Pool.register(
        service_order.ServiceOrderReport,
        service_order.DetailedBillingAnalysisReport,
        module='laboratory', type_='report')
