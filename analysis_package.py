# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal

from trytond.exceptions import UserError
from trytond.model import ModelView, ModelSQL, fields
from trytond.modules.product import round_price
from trytond.pool import Pool
from trytond.pyson import Eval, If, In, Get
from trytond.transaction import Transaction


class AnalysisPackage(ModelSQL, ModelView):
    "Analysis Package"
    __name__ = "laboratory.analysis_package"
    name = fields.Char('Name', required=True)
    company = fields.Many2One(
        'company.company', 'Company', required=True,
        domain=[('id', If(
            In('company', Eval('context', {})), '=', '!='),
            Get(Eval('context', {}), 'company', 0))])
    customer = fields.Many2One('party.party', 'Customer')
    price_list = fields.Many2One('product.price_list', 'Price List',
                                 states={'required': True})
    lines = fields.One2Many('laboratory.analysis_package.line',
                            'analysis_package', 'Lines')
    total_amount = fields.Function(fields.Numeric('Total Amount',
                                                  digits=(16, 2)), 'get_total_amount')

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or False
    # @classmethod
    # def __setup__(cls):
    #     super(AnalysisPackage, cls).__setup__()
    #     cls._order.insert(0, ('name', 'ASC'))

    # def get_rec_name(self, name):
    #     if self.parent:
    #         return self.parent.get_rec_name(name) + ' / ' + self.name
    #     else:
    #         return self.name

    def get_total_amount(self, name=None):
        res = []
        for line in self.lines:
            res.append(line.unit_price)
        return sum(res)


class AnalysisPackageLine(ModelSQL, ModelView):
    "Analysis Package Line"
    __name__ = "laboratory.analysis_package.line"
    analysis_package = fields.Many2One('laboratory.analysis_package',
                                       'Analysis Package', required=True)
    test = fields.Many2One('product.product', 'Test', required=True,
                           domain=[('salable', '=', True)])
    unit_price = fields.Numeric(
        'Unit Price', states={'required': True})

    @fields.depends(
        'test', 'unit_price',
        methods=['compute_unit_price'])
    def on_change_test(self):
        pool = Pool()
        price_list_id = None
        if self.analysis_package.price_list:
            price_list_id = self.analysis_package.price_list.id
            if self.test:
                PriceList = pool.get('product.price_list')
                price_list = PriceList(price_list_id)
                p_list = [line.product for line in price_list.lines]
                if self.test in p_list:
                    self.unit_price = self.compute_unit_price()
                else:
                    raise UserError('No Esta en la lista de precios')
            else:
                raise UserError('Ya fue agregado el examen')
        else:
            raise UserError('No hay una lista de precios')

    @fields.depends(
        'test',
        'analysis_package', '_parent_analysis_package.customer',
        '_parent_analysis_package.price_list',
        '_parent_analysis_package.company')
    def compute_unit_price(self):
        pool = Pool()
        Product = pool.get('product.product')

        if not self.test:
            return 0
        context = {}
        if self.analysis_package:
            if self.analysis_package.customer:
                context['customer'] = self.analysis_package.customer.id
            if self.analysis_package.company:
                context['currency'] = self.analysis_package.company.currency.id
                context['company'] = self.analysis_package.company.id
            if self.analysis_package.price_list:
                print(self.analysis_package.price_list.name)
                context['price_list'] = self.analysis_package.price_list.id

        context['uom'] = self.test.sale_uom.id

        with Transaction().set_context(context):
            unit_price = Product.get_sale_price([self.test],
                                                1 or 0)[self.test.id]
            if unit_price:
                unit_price = round_price(unit_price)
            return unit_price
