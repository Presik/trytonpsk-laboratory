# # This file is part of Tryton.  The COPYRIGHT file at the top level of
# # this repository contains the full copyright notices and license terms.
# from trytond.model import ModelView, ModelSQL, fields


# STATES = {'readonly': True}


# class ApiLog(ModelSQL, ModelView):
#     "API Log"
#     __name__ = "api.log"
#     endpoint = fields.Char("Endpoint", states=STATES)
#     number = fields.Char('Number Doc', states=STATES)
#     reference = fields.Char('Order Reference', states=STATES)
#     record_date = fields.Date('Record Date', states=STATES)
#     msg_response = fields.Char('Msg Response', states=STATES)
#     request_json = fields.Text("Request Json", states=STATES)
#     status = fields.Char('Status', states=STATES)

#     @classmethod
#     def __setup__(cls):
#         super(ApiLog, cls).__setup__()
#         cls._order.insert(0, ('record_date', 'DESC'))
