# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction
from trytond.pyson import Eval, Id


class Configuration(ModelSQL, ModelView):
    'Laboratory Configuration'
    __name__ = 'laboratory.configuration'
    laboratory_service_order_sequence = fields.Many2One('ir.sequence',
        'Service Order Sequence', required=True,
        domain=[
            ('company', 'in',
                [Eval('context', {}).get('company', -1), None]),
            ('sequence_type', '=',
                Id('laboratory',
                    'sequence_type_laboratory')),
            ])
    company = fields.Many2One('company.company', 'Company', required=True)
    copago_product = fields.Many2One('product.product', 'Product Copago',
        domain=[('salable', '=', True)])
    cuota_moderadora_product = fields.Many2One('product.product',
        'Cuota Moderadora', domain=[('salable', '=', True)])
    delivery_product = fields.Many2One('product.product', 'Product Delivery',
        domain=[('salable', '=', True)])
    account_discount = fields.Many2One('account.account', "Account Discount")
    account_courtesy = fields.Many2One('account.account', "Account Courtesy")
    validate_invoice = fields.Boolean('Validate Invoice')
    bill_copago = fields.Boolean('Bill Copago')
    advance_accounts = fields.Many2One('account.account', "Advance Accounts")
    group_invoice_lines = fields.Boolean('Group Invoice Lines')
    doctor_category = fields.Many2One('party.category', 'Doctor Category')

    @staticmethod
    def default_group_invoice_lines():
        return False

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @classmethod
    def get_configuration(cls):
        res = cls.search([
            ('company', '=', int(Transaction().context.get('company')))
        ])
        return res[0]
