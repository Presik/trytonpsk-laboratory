# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.model import fields, ModelSQL
from trytond.pool import PoolMeta, Pool

_ZERO = Decimal('0.00')


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'
    annexes = fields.Many2Many('account.invoice-laboratory.order', 'invoice',
        'order', 'annexes')
    copago = fields.Numeric('Copago', digits=(16, 2), depends=['annexes'])
    cuota_moderadora = fields.Numeric('Cuota Moderadora', digits=(16, 2), depends=['annexes'])

    def _get_move_line(self, date, amount):
        line = super(Invoice, self)._get_move_line(date, amount)
        if self.copago and self.operation_type != 'SS‐CUFE':
            print('line.debit = line.debit - self.copago')
            line.debit = line.debit - self.copago
        return line

    def get_move(self):
        move = super(Invoice, self).get_move()
        Config = Pool().get('laboratory.configuration')
        config = Config.get_configuration()
        if (self.copago or self.cuota_moderadora) and self.operation_type != 'SS‐CUFE' and config.copago_product:
            move.lines = list(move.lines) + [self.get_copago_line()]
        return move

    def get_copago_line(self):
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        Config = pool.get('laboratory.configuration')
        config = Config.get_configuration()
        line = MoveLine()
        line.amount_second_currency = None
        line.second_currency = None
        amount_ = (self.copago or 0) + (self.cuota_moderadora or 0)
        if self.total_amount <= 0:
            line.debit, line.credit = 0, amount_
        else:
            line.debit, line.credit = amount_, 0

        line.operation_center = self.operation_center.id
        account = config.copago_product.template.account_category.account_revenue
        line.account = account
        if account.party_required:
            line.party = self.party
        line.description = config.copago_product.rec_name
        return line

    @fields.depends('annexes', 'copago', 'cuota_moderadora')
    def on_change_annexes(self, name=None):
        print('ingresa a este valor', self.annexes)
        if self.annexes:
            self.copago = sum((a.copago or 0) for a in self.annexes)
            self.cuota_moderadora = sum((a.cuota_moderadora or 0) for a in self.annexes)


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    @classmethod
    def _get_origin(cls):
        models = super()._get_origin()
        models.append('laboratory.order.line')
        return models


class InvoiceLaboratoryOrder(ModelSQL):
    "Invoice - Order"
    __name__ = "account.invoice-laboratory.order"
    _table = 'account_invoice_laboratory_order'

    invoice = fields.Many2One('account.invoice', 'Invoice',
        ondelete='CASCADE', select=True, required=True)
    order = fields.Many2One('laboratory.order', 'Laboratory Order',
        ondelete='RESTRICT', select=True, required=True)
