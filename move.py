from trytond.pool import PoolMeta
from trytond.modules.analytic_co.exceptions import AnalyticError
from trytond.i18n import gettext


class Move(metaclass=PoolMeta):
    __name__ = 'account.move'

    @classmethod
    def validate_move(cls, moves):
        super(Move, cls).validate_move(moves)
        for move in moves:
            if move.state != 'draft' and move.period.type != 'adjustment':
                for line in move.lines:
                    if line.account and line.account.type.statement != 'balance' and \
                        not line.analytic_lines and not line.account.code.startswith('4'):
                        raise AnalyticError(
                            gettext('analytic_co.msg_analytic_error',
                            line=line.rec_name, account=line.account.rec_name)
                        )