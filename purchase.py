from trytond.pool import PoolMeta
from trytond.model import fields
from itertools import chain


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    @fields.depends('party', 'invoice_party', 'payment_term', 'lines')
    def on_change_party(self):
        super(Purchase, self).on_change_party()
        self.invoice_method = 'shipment'

    def create_invoice(self):
        'Create an invoice for the purchase and return it'

        if self.invoice_method == 'manual':
            return

        goods_to_invoice = []
        line_goods = []
        invoice_lines = []
        for line in self.lines:
            invoice_line = line.get_invoice_line()
            if line.product.type == 'goods':
                line_goods.append(line.id)
                if invoice_line:
                    goods_to_invoice.append(line.id)
            invoice_lines.append(invoice_line)
        invoice_lines = list(chain(*invoice_lines))
        if not invoice_lines or self.invoice_method == 'shipment' and len(line_goods) > 0 and len(goods_to_invoice) <= 0:
            return

        invoice = self._get_invoice_purchase()
        if getattr(invoice, 'lines', None):
            invoice_lines = list(invoice.lines) + invoice_lines
        invoice.lines = invoice_lines
        invoice.save()
        invoice.update_taxes()
        self.copy_resources_to(invoice)
        return invoice


class Line(metaclass=PoolMeta):
    __name__ = 'purchase.line'

    def get_invoice_line(self):
        lines = super(Line, self).get_invoice_line()
        if lines:
            line = lines[0]
            if self.moves:
                move = self.moves[-1]
                line.unit_price = move.unit_price
                lines = [line]
        return lines