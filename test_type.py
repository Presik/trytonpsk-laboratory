# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from __future__ import with_statement

from trytond.model import Workflow, ModelView, ModelSQL, fields


class TestType(Workflow, ModelSQL, ModelView):
    'Test Type'
    __name__ = 'laboratory.test_type'
    _rec_name = 'product'
    code = fields.Char('Code')
    product = fields.Many2One('product.product', 'Product', required=True,
        domain=[
            ('template.type', '=', 'service'),
            ('template.salable', '=', True),
        ])
    category = fields.Many2One('laboratory.category', 'Category', required=True,
        select=True)
    description = fields.Char('Description')
    active = fields.Boolean('Active')

    def get_rec_name(self, name):
        if self.product:
            return self.product.rec_name
