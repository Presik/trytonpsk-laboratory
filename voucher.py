from trytond.pool import PoolMeta
import requests
import json


HEADERS = {
           'Accept': 'application/json',
           'Content-type': 'application/json',
           }


class Voucher(metaclass=PoolMeta):
    __name__ = 'account.voucher'

    @classmethod
    def process(cls, records):
        super(Voucher, cls).process(records)
        for voucher in records:
            if voucher.voucher_type == 'receipt':
                is_person = True
                if voucher.party.type_document == '31':
                    is_person = False
                params = {
                    'is_person': is_person,
                    'id_number': voucher.party.id_number,
                    'total_credit': str(voucher.party.receivable),
                    }
                request = json.dumps(params)
                uri = 'http://d4450c187272.sn.mynetname.net:18001/cupoempresa'
                requests.post(uri, auth=('tryton', 'try70#Pc&1'),
                              headers=HEADERS, data=request)
